package ${servicePkg};

<#if modelPkg?? >
import ${modelPkg}.${nickName?cap_first};
</#if>
<#if daoPkg?? >
import ${daoPkg}.${nickName?cap_first}Dao;
</#if>
<#if servicePkg?? >
import ${servicePkg}.${nickName?cap_first}Service;
</#if>
import cn.jfast.framework.web.annotation.Resource;

<#assign gentime = .now>
/**
 * ${nickName}模块对应的业务接口
 * @since ${gentime?string['yyyy-MM-dd HH:mm']}
 * @author jfast
 */
@Resource(name="${nickName?uncap_first}Service")
public class ${nickName?cap_first}ServiceImpl implements ${nickName?cap_first}Service{
	
	@Resource
	private ${nickName?cap_first}Dao ${nickName?uncap_first}Dao;
	/**
	 * 查询${nickName}详情
	 */
	public ${nickName?cap_first} get${nickName?cap_first}By${primaryKeyTemplate}(${primaryParamTemplate}){
		return ${nickName?uncap_first}Dao.get${nickName?cap_first}By${primaryKeyTemplate}(${primaryValueTemplate});
	}
	
	/**
	 * 删除${nickName}对象
	 */
	public void delete${nickName?cap_first}By${primaryKeyTemplate}(${primaryParamTemplate}){
		${nickName?uncap_first}Dao.delete${nickName?cap_first}By${primaryKeyTemplate}(${primaryValueTemplate});
	}
	
	/**
	 * 更新${nickName}对象
	 */
	public void update${nickName?cap_first}By${primaryKeyTemplate}(${nickName?cap_first} ${nickName?uncap_first}){
		${nickName?uncap_first}Dao.update${nickName?cap_first}By${primaryKeyTemplate}(${nickName?uncap_first});
	}
	
	/**
	 * 添加${nickName}对象
	 */
	public void add${nickName?cap_first}(${nickName?cap_first} ${nickName?uncap_first}){
		${nickName?uncap_first}Dao.add${nickName?cap_first}(${nickName?uncap_first});
	}

}