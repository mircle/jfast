package ${controllerPkg};

import cn.jfast.framework.log.LogFactory;
import cn.jfast.framework.log.LogType;
import cn.jfast.framework.log.Logger;
import cn.jfast.framework.web.annotation.Api;
import cn.jfast.framework.web.annotation.Get;
import cn.jfast.framework.web.annotation.HttpParam;
import cn.jfast.framework.web.annotation.Post;
import cn.jfast.framework.web.annotation.Put;
import cn.jfast.framework.web.annotation.Delete;
import cn.jfast.framework.web.annotation.Resource;
import cn.jfast.framework.web.view.viewtype.Json;
<#if servicePkg?? >
import ${servicePkg}.${nickName?cap_first}Service;
</#if>
<#if modelPkg?? >
import ${modelPkg}.${nickName};
</#if>
<#assign gentime = .now>
/**
 * ${nickName}模块对应控制器
 * @since ${gentime?string['yyyy-MM-dd HH:mm']}
 * @author jfast
 */
@Api(path = "api/v1", description = "${nickName}控制器")
public class ${nickName?cap_first}Controller {

	public static Logger log = LogFactory.getLogger(LogType.JFast,${nickName?cap_first}Controller.class);
	@Resource
	private ${nickName?cap_first}Service ${nickName?uncap_first}Service;
	
	/**
	* 插入
	*/
	@Post(path = "${nickName?uncap_first}s", description = "添加新${nickName}信息")
	public Json add${nickName?cap_first}(@HttpParam(fromJson=true) ${nickName?cap_first} ${nickName?uncap_first}){
		${nickName?uncap_first}Service.add${nickName?cap_first}(${nickName?uncap_first});
		return new Json("{errCode:0,errMsg:'',data:''}");
	}
	
	/**
	* 查询
	*/
	@Get(path = "${nickName?uncap_first}s${primaryPathRouteTemplate}", description = "查询指定主键的${nickName}信息")
	public Json get${nickName?cap_first}(${primaryParamTemplate}){
		${nickName?uncap_first}Service.get${nickName?cap_first}By${primaryKeyTemplate}(${primaryValueTemplate});
		return new Json("{errCode:0,errMsg:'',data:''}");
	}
	
	/**
	* 更新
	*/
	@Put(path = "${nickName?uncap_first}s${primaryPathRouteTemplate}", description = "更新指定主键的${nickName}信息")
	public Json update${nickName?cap_first}(@HttpParam(fromJson=true) ${nickName?cap_first} ${nickName?uncap_first}){
		${nickName?uncap_first}Service.update${nickName?cap_first}By${primaryKeyTemplate}(${nickName?uncap_first});
		return new Json("{errCode:0,errMsg:'',data:''}");
	}
	
	/**
	* 删除
	*/
	@Delete(path = "${nickName?uncap_first}s${primaryPathRouteTemplate}", description = "更新指定主键的${nickName}信息")
	public Json delete${nickName?cap_first}(${primaryParamTemplate}){
		${nickName?uncap_first}Service.delete${nickName?cap_first}By${primaryKeyTemplate}(${primaryValueTemplate});
		return new Json("{errCode:0,errMsg:'',data:''}");
	}
	
}