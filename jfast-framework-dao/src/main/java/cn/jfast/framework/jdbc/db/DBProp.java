/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.jdbc.db;

import cn.jfast.framework.base.cache.Cache;
import cn.jfast.framework.base.cache.cacheimpl.ConcurrentMapCache;
import cn.jfast.framework.jdbc.info.Table;

/**
 * jdbc属性
 *
 * @author 泛泛o0之辈
 */
public class DBProp {

	public static Cache tableCache = new ConcurrentMapCache("table");
	public static Cache modelCache = new ConcurrentMapCache("msg");
	public static String jdbc_url = "";
	public static String jdbc_user = "";
	public static String jdbc_password = "";
	public static String jdbc_driver = "";
	public static int max_pool_size = 100;
	public static int min_pool_size = 10;
	public static int initial_pool_size = 10;
	public static int max_idle_time = 20;
	public static int acquire_increment = 4;
	public static String DB_NAME = "";
	public static String DB_VERSION = "";
	public static String DB_DRIVER = "";
	
	public static String getJdbcUrl(){
    	if(DBProp.jdbc_url.contains("?")){
    		return DBProp.jdbc_url+"&generateSimpleParameterMetadata=true";
    	} else {
    		return DBProp.jdbc_url+"?generateSimpleParameterMetadata=true";
    	}
    }
	
	public static Table getTable(String tableName){
		if(null != modelCache.get(tableName.toLowerCase()))
			tableName = (String) modelCache.get(tableName.toLowerCase());
		return tableCache.get(tableName.toLowerCase(),Table.class);
	}
}
