package cn.jfast.framework.web.view.viewtype;

import cn.jfast.framework.web.view.View;

public class Image extends View{
	
	private byte[] view;
	
	public Image(byte[] img){
		this.view = img;
	}
		
	@Override
	public byte[] getView() {
		return view;
	}
	
	@Override
	public String toString() {
		return "Image";
	}

}
