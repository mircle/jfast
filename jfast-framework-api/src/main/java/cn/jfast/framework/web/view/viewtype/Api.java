/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.web.view.viewtype;

import cn.jfast.framework.web.view.View;

import java.util.HashMap;
import java.util.Map;

public class Api extends View {

    private final String action;
    Map<String, String> actionAttrs = new HashMap<String, String>();

    public Api(String action) {
        this.action = format(action);
    }

    private String format(String action) {
        action = ("/" + action + "/").replaceAll("/+", "/");
        if (action.endsWith("/")) {
            action = action.substring(0, action.length() - 1);
        }
        return action;
    }

    public Map<String, String> getActionAttrs() {
        return actionAttrs;
    }

    public Api addAttr(String attrName, String attrValue) {
        actionAttrs.put(attrName, attrValue);
        return this;
    }

    public Api addAttrs(Map<String, String> attrs) {
        actionAttrs.putAll(attrs);
        return this;
    }

    @Override
    public String getView() {
        return this.action;
    }

    @Override
    public String toString() {
        return "api:" + this.getView();
    }
}
