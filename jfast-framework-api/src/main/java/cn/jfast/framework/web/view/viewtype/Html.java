package cn.jfast.framework.web.view.viewtype;

import cn.jfast.framework.web.view.View;

/**
 * 返回指定Html页面
 */
public class Html extends View {

    private String view;

    public Html(String view){
        this.view = view;
    }
    
    public Html addAttr(String paramName,String paramValue){
    	if(view.indexOf("?") != -1){
    		view += "&"+paramName+"="+paramValue;
    	} else {
    		view += "?"+paramName+"="+paramValue;
    	}
    	return this;
    }
    @Override
    public String getView() {
        return this.view;
    }

    @Override
    public String toString() {
        return "html:"+this.getView();
    }
}
