JFast : Java Web MVC开发框架 + Restful服务端框架
==============================================================

**使用步骤:

##### 为了超速开发,请先添加代码生成插件到Eclipse中

##### 创建项目目录比如: cn.demo.dao/ cn.demo.service/ cn.demo.action/ cn.demo.model;

##### 选择Eclipse菜单 : JFast --> Auto Generate,配置数据库信息，自动生成代码;

##### 项目基本搞定，开发者只要专注于业务即可。